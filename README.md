# Community Base Addons

## Backport

This is a backport of Community Base Addons to be compatible with Arma 1.82.
This is the version that the native Linux client uses. If you do not know what
this means, you probably don't need this.

Typically, there are two categories of changes made to backport CBA: removal of
references to new items, objects, vehicles, units, etc., and the rewriting of
scripts to avoid commands introduced in a later version.

## Installation

Download the latest version and unpack it in your Arma 3 installation folder.
Simply launch Arma 3 with `-mod=@CBA_A3` afterwards.

## Optionals

To install any of the optionals, simply copy the respective PBOs into the `@CBA_A3\addons` folder.

PBO                                    | Description
-------------------------------------- | --------------------------------------
cba_cache_disable.pbo                  | Disables CBA's function caching. (Dev Tool)
cba_diagnostic_disable_xeh_logging.pbo | Disables all additional XEH RPT logging.
cba_diagnostic_enable_logging.pbo      | Enables additional logging (Dev Tool)

### CBA Caching

CBA implements a cache for all `compile preProcessFile`'d scripts and for all XEH events on `CfgVehicle` classes. This cache is stored in the `uiNamespace` and is therefore available throughout the whole lifetime of the running game (game start, till terminate game). Each class is only cached once, while mission and `campaignConfigfile` events are evaluated every mission, but also only once per `CfgVehicle` class.

The performance gains are seen in feature rich mods like ACE3 which rely heavily on scripting to make their features possible.
Some of these functionalities cause long loading times for the game, switching missions, islands and switching from the editor back to the game.
At the start of the game functions will be cached and loading times for functions will be comparable with the vanilla game.

`cba_cache_disable.pbo` is an optional addon that can disable this if you need it. However it makes mods slower by disabling CBA's function and script compilation cache, as well as the XEH cache. It is useful during development, since script changes will take effect without restarting the entire game.

## Known Issues

* CBA Keybindings and Settings require a mission to be initialized to function properly. This includes working in the main menu of Arma 3. Commandline parameters like `-world=empty` or `-skipIntro` will cause Keybindings and Settings to work ONLY in-game but NOT in the main menu.

## License

Licensed under GNU General Public License ([GPLv2](LICENSE.md))

Any addon which calls CBA-defined functions need not be licensed under the GPLv2 or released under a free software license. Only if it is directly including CBA code in the addon's binarized PBO or redistributing a modified version of CBA itself would it be considered derivative and therefore be legally required to be released under the terms of the GPL. (And there's no reason to ever do either of these.)
